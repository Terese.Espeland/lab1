package rockPaperScissors;

import java.util.Arrays;
import java.util.Random;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
    	while (true) {
    		System.out.println("Let's play round " + roundCounter);
    		String humanChoice = userChoice();
    		String computerChoice = computerInput();
    		String choiceString = ("Human chose " + humanChoice + ", computer chose " + computerChoice + ".");
    	
    		if (isWinner(humanChoice, computerChoice)) {
    			System.out.println(choiceString + " Human wins!");
    			humanScore += 1;
    		}
    		else if (isWinner(computerChoice, humanChoice)) {
    			System.out.println(choiceString + " Computer wins!");
    			computerScore +=1;
    		}
    		else {
    			System.out.println(choiceString + " It's a tie!");
    		}
    		
    		System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    		
    		String continueAnswer = continuePlaying();
    		
    		if (continueAnswer.equals("n")) {
    			break;
    		} else {
    			roundCounter += 1;
    		}
    		
    	}
    	System.out.println("Bye bye :)");
    	
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    
    public String computerInput() {
        Random rand = new Random();
    	String computer = rpsChoices.get(rand.nextInt(rpsChoices.size()));
        return computer;
    }
    
    public String userChoice() {
    	while (true) {
    		String userChoice = readInput("Your choice (Rock/Paper/Scissors)?");
    		if (rpsChoices.contains(userChoice)) {
    			return userChoice;
    		} else {
    			System.out.println("I do not understand " + userChoice + ". Could you try again?");
    		}
    	}
    }
    
    public Boolean isWinner(String choice1, String choice2) {
    	if (choice1.equals("paper")) {
    		return (choice2.equals("rock"));
    	} else if (choice1.equals("scissors")) {
    		return (choice2.equals("paper"));
    	} else {
    		return (choice2.equals("scissors"));
    	}
    }
    
    public String continuePlaying() {
    	while (true) {
    		String continueAnswer = readInput("Do you wish to continue playing? (y/n)?");
    		List<String> answers = Arrays.asList("y", "n");
    		if (answers.contains(continueAnswer)) {
    			return (continueAnswer);
    		} else {
    			System.out.println("I don't understand " + continueAnswer + ". Try again");
    		}
    	}
    }

}
